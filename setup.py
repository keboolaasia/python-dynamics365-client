from setuptools import setup, find_packages


setup(
    name='dynamics365',
    use_scm_version=True,
    setup_requires=['setuptools-scm', 'pytest-runner'],
    url='https://bitbucket.org/keboolaasia/python-dynamics365-client',
    download_url='https://bitbucket.org/keboolaasia/python-dynamics365-client',
    packages=find_packages(exclude=['tests']),
    test_suite='tests',
    tests_require=['pytest'],
    install_requires=['requests']
)
