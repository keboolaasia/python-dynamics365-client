"""

The client
- handles authentication and tokens
- makes authenticated requests

We need to save the refresh token to statefile and be able to reuse it from statefile


"""
from urllib.parse import urlencode, parse_qs, urlparse
import logging
import requests
from requests.packages.urllib3.util.retry import Retry
from requests.adapters import HTTPAdapter

class DynamicsClient(requests.Session):
    """
    The client
    - handles authentication and tokens
    - makes authenticated requests

    """

    def __init__(self, resource, access_token=None, refresh_token=None, client_id=None, client_secret=None, redirect_uri=None, retries=3, retry_backoff_factor=0.3, retry_status_forcelist=(500, 502, 504)):
        super().__init__()
        self.client_id = client_id
        self.client_secret = client_secret
        self.resource = resource
        self.base_url = resource.rstrip('/') + '/api/data/v9.0/'
        self.redirect_uri = redirect_uri
        self._token_url = 'https://login.microsoft.com/common/oauth2/token'

        self._access_token = access_token
        self._refresh_token = refresh_token

        # for convenience
        self.urlencode = urlencode

        # https://www.peterbe.com/plog/best-practice-with-retries-with-requests
        _retry = Retry(total=retries,
                       read=retries,
                       connect=retries,
                       backoff_factor=retry_backoff_factor,
                       status_forcelist=retry_status_forcelist,
        )
        _adapter = HTTPAdapter(max_retries=_retry)
        self.mount('http://', _adapter)
        self.mount('https://', _adapter)

    @property
    def access_token(self):
        if self._access_token is None:
            self._refresh_access_token()
        return self._access_token

    @access_token.setter
    def access_token(self, value):
        self._access_token = value

    @property
    def refresh_token(self):
        return self._refresh_token

    @refresh_token.setter
    def refresh_token(self, value):
        self._refresh_token = value

    @property
    def authorization_url(self):
        return ("https://login.microsoftonline.com/common/oauth2/authorize?"
                "response_type=code"
                "&client_id={client_id}"
                "&redirect_uri={redirect_uri}"
                "&state=1234"
                "&resource={resource}").format(
                    resource=self.resource,
                    client_id=self.client_id,
                    redirect_uri=self.redirect_uri)

    def tokens_from_authorization_code(self, response_url):
        """
        Parse code from post authorization url and exchange for access/refresh_tokens

        Args:
            respone_url (str): the full fledged response url

        Returns:
            Nothing on success. Find the tokens in self.refresh_token and
                self.access_token

        """

        logging.info("Exchanging authorization_code for auth tokens")
        authorization_code = parse_qs(urlparse(response_url).query,
                                      keep_blank_values=True)['code'][0]

        payload = {
            'client_id': self.client_id,
            'redirect_uri': self.redirect_uri,
            'client_secret': self.client_secret,
            'grant_type': 'authorization_code',
            'code': authorization_code
        }
        resp = requests.post(self._token_url, data=payload)

        try:
            resp.raise_for_status()
        except requests.exceptions.HTTPError as err:
            logging.error(err.response.text)
            raise
        else:
            data = resp.json()
            logging.info("Setting access_token and refresh_token")
            self.refresh_token = data['refresh_token']
            self.access_token = data['access_token']

    def _refresh_access_token(self):
        logging.debug("Getting new access_token")
        payload = {
            'client_id': self.client_id,
            'redirect_uri': self.redirect_uri,
            'client_secret': self.client_secret,
            'grant_type': 'refresh_token',
            'refresh_token': self.refresh_token
        }

        resp = requests.post(self._token_url, data=payload)
        try:
            resp.raise_for_status()
        except:
            logging.error(resp.text)
            raise
        else:
            auth_data = resp.json()
            self.access_token = auth_data['access_token']
            self.refresh_token = auth_data['refresh_token']

    def _build_url(self, endpoint, entity_id=None, suffix=None):
        url = self.base_url + endpoint.strip('/')
        if entity_id is not None:
            url += '({})'.format(entity_id)
        if suffix is not None:
            url += '/{}'.format(suffix)
        return url

    def get_one(self, endpoint):
        """
        Get one single endpoint
        for example "contacts(1234-1234-1234-1234-1234)"

        """

        url = self._build_url(endpoint)
        return self._request("GET", url).json()

    def get(self, endpoint, params=None):
        """
        Get data from all pages of an endpoint

        Args:
            endpoint (str): for example "accounts"
        Returns:
            a generator yielding the actual rows of the dataset

        """
        next_page = self._build_url(endpoint)
        while next_page is not None:
            data = self._request("GET", next_page, params=params).json()
            for row in data['value']:
                yield row
            next_page = data.get('@odata.nextLink')
            # after first request, "delete" params
            # because next pages already include them
            params=None

    def create(self, endpoint, json=None):
        """
        Create a new entity

        """
        url = self._build_url(endpoint)
        return self._request('POST', url, json=json)

    def update_property(self, endpoint, entity_id, property_name, value):
        """
        Update single entity property

        """
        data = {"value": value}
        url = self._build_url(endpoint, entity_id, suffix=property_name)
        return self._request('PUT', url, json=data)

    def upsert(self, endpoint, entity_id, json):
        """
        UPSERT an entity with the json payload

        """
        url = self._build_url(endpoint, entity_id)
        return self._request('PATCH', url, json=json)

    def delete(self, endpoint, entity_id):
        """
        UPSERT an entity with the json payload

        """
        url = self._build_url(endpoint, entity_id)
        return self._request('DELETE', url)

    def update(self, endpoint, entity_id, json):
        """
        Update or fail

        https://msdn.microsoft.com/en-us/library/mt607711.aspx#bkmk_limitUpsertOperations

        If the entity is found, you’ll get a normal response
        with status 204 (No Content).
        When the entity is not found,
        you’ll get the following response with status 404 (Not Found).
        """

        url = self._build_url(endpoint, entity_id)
        return self._request('PATCH', url, json=json, headers={'If-Match': '*'})

    def _request(self, method, url, **kwargs):
        """
        Make a simple authenticated request with correct headers

        If access_token expires, try again (once) with refreshed token.

        Args:
            url (str): resource url
            retry (bool): Whether to retry on unauthorized request.
        Returns:
            a json response

        """
        headers = {
            'Authorization': 'Bearer {}'.format(self.access_token),
            'Accept': 'application/json OData-MaxVersion: 4.0 OData-Version: 4.0',
        }
        try:
            extra_headers = kwargs.pop('headers')
        except KeyError:
            # no extra headers
            pass
        else:
            headers.update(extra_headers)
        retry = kwargs.pop('retry', True)
        resp = self.request(method, url, headers=headers, **kwargs)
        try:
            resp.raise_for_status()
        except requests.exceptions.HTTPError as err:
            # try again with refreshed token
            # But try only once otherwise infinite loop - boom
            if err.response.status_code == 401 and retry:
                logging.debug("Access token invalid? Retrying")
                self._refresh_access_token()
                return self._request(method, url, retry=False, **kwargs)
            else:
                logging.error(err)
                raise
        else:
            return resp
