from pkg_resources import get_distribution, DistributionNotFound
try:
    release = get_distribution(__name__).version
    __version__ = '.'.join(release.split('.')[:3])
except DistributionNotFound:
    # package is not installed
    pass

from dynamics365.client import DynamicsClient
