import os
import pytest
import re
import types
from dynamics365 import DynamicsClient
import random
import requests
import logging

def refresh_auth_context():
    return {
        'client_secret': os.environ['CL_CLIENT_SECRET'],
        'client_id': os.environ['CL_CLIENT_ID'],
        'redirect_uri': os.environ['CL_REDIRECT_URI'],
        'resource': os.environ['CL_RESOURCE'],
        'refresh_token': os.environ.get('CL_REFRESH_TOKEN'),
    }

@pytest.fixture(scope='module')
def authenticated_client():
    api = DynamicsClient(**refresh_auth_context())
    api.access_token
    return api

def test_unauthorized_request_refresh_token_and_retry():
    api = DynamicsClient(**refresh_auth_context())
    api.access_token = 'foobar-invalid'

    # should retry on 401
    resp = api._request('GET', api.base_url + '?$top=1')
    assert resp.status_code == 200
    assert api.access_token != 'foobar-invalid'


def test_getting_access_token():
    api = DynamicsClient(**refresh_auth_context())
    assert api._access_token is None
    assert isinstance(api.access_token, str) and len(api.access_token) > 10


def test_getting_all_records(authenticated_client):
    endpoints = authenticated_client.get('')
    assert isinstance(endpoints, types.GeneratorType)
    endpoint = next(endpoints)
    assert isinstance(endpoint, dict)
    assert 'url' in endpoint
    assert 'kind' in endpoint
    assert 'name' in endpoint
    assert len(list(endpoint)) > 1



# Against a sandbox I can create an account, let's try the whole workflow

def test_whole_workflwo(authenticated_client):
    random_nonce = str(random.randint(0,2**32))
    create_msg = "KEBOOLA CLIENT TEST CREATE"
    update_msg = "KEBOOLA CLIENT UPDATED"

    new_data = {
        "esp_name": "CAN BE DELETED",
        "esp_kbc_custom_field_1": random_nonce,
        "esp_kbc_description": create_msg
}
    resp = authenticated_client.create('esp_kbc_testings', new_data)

    match = re.search(r'\((?P<entity_id>\w+-\w+-\w+-\w+-\w+)\)',
                      resp.headers['Location']
          )
    eid = match.groupdict()['entity_id']
    print("New contact {} created".format(eid))

    created = authenticated_client.get_one('esp_kbc_testings({})'.format(eid))
    assert created['esp_kbc_custom_field_1'] == random_nonce
    assert created['esp_kbc_description'] == create_msg

    # try updating
    updated_resp = authenticated_client.update('esp_kbc_testings', eid, {'esp_kbc_description': update_msg })
    assert updated_resp.status_code == 204, updated_resp.content

    updated = authenticated_client.get_one('esp_kbc_testings({})?'.format(eid))
    assert updated['esp_kbc_custom_field_1'] == random_nonce
    assert updated['esp_kbc_description'] == update_msg

    deleted = authenticated_client.delete('esp_kbc_testings', eid)
    assert deleted.status_code == 204, deleted.content

    with pytest.raises(requests.HTTPError) as err:
        # make sure it doesn't exist
        authenticated_client.get_one('esp_kbc_testings({})'.format(eid))
        assert err.response.status_code == 404


