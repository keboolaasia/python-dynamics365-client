FROM frolvlad/alpine-python3:latest
RUN apk add --no-cache git
RUN pip install --no-cache-dir --ignore-installed git+https://bitbucket.org/keboolaasia/python-dynamics365-client.git@0.1.1
